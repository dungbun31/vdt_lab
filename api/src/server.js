const express = require("express");
const app = express();
const port = process.env.NODE_DOCKER_PORT || 8080;
const Students = require("./students");
const sequelize = require("./database");
const cors = require("cors");

var corsOptions = {
  origin: process.env.CLIENT_ORIGIN || "http://localhost:8080",
};

app.use(cors(corsOptions));

// app.use(
//   cors({
//     origin: "*",
//     credentials: true,
//     optionSuccessStatus: 200,
//   })
// );

sequelize.sync().then(() => {
  console.log("Database connected");
});

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Route GET to fetch all students
app.get("/api/students", async (req, res) => {
  try {
    const students = await Students.findAll();
    res.json(students);
  } catch (error) {
    console.error("Error fetching students:", error);
    res.status(500).json({ error: "Failed to fetch students" });
  }
});
 
// Route POST to create a new student
app.post("/api/students", async (req, res) => {
  try {
    await Students.create(req.body);
    res.status(201).json({ success: true });
  } catch (error) {
    console.error("Error adding student:", error);
    res.status(500).json({ error: "Failed to add student" });
  }
});

// Route PUT to update student by ID
app.put("/api/students/:id", async (req, res) => {
  try {
    const student = await Students.findByPk(req.params.id);
    if (!student) {
      return res.status(404).json({ error: "Student not found" });
    }
    await Students.update(req.body, { where: { id: req.params.id } });
    res.json({ success: true });
  } catch (error) {
    console.error("Error updating student:", error);
    res.status(500).json({ error: "Failed to update student" });
  }
});

// Route DELETE to delete student by ID
app.delete("/api/students/:id", async (req, res) => {
  try {
    const student = await Students.findByPk(req.params.id);
    if (!student) {
      return res.status(404).json({ error: "Student not found" });
    }
    await Students.destroy({ where: { id: req.params.id } });
    res.json({ success: true });
  } catch (error) {
    console.error("Error deleting student:", error);
    res.status(500).json({ error: "Failed to delete student" });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

